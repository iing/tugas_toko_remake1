 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url();?>assets/Admin_LTE/dist/img/IMG-20180718-WA0003.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>iing sodikin ik21</p>
          <a href="#"><i class="fa fa-circle text-success"></i>ofline</a>
        </div>
      </div>
      <!-- search form -->
      <form action="<?=base_url()?>karyawan/listkaryawan" method="POST" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="caridata" id="Cari data" autocomplete="off" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="tombol_cari" id="search-btn" value="Cari data" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">TOKO JAYA ABADI</li>
       
        
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Menu Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
         <li><a href="<?=base_url();?>karyawan/listkaryawan"><i class="fa fa-circle-o text-red"></i><span>list karyawan</span></a></li>
         <li><a href="<?=base_url();?>jabatan/listjabatan"><i class="fa fa-circle-o text-red"></i><span>list jabatan</span></a></li>
          <li><a href="<?=base_url();?>barang/listbarang"><i class="fa fa-circle-o text-red"></i><span>list barang</span></a></li>
           <li><a href="<?=base_url();?>jenis_barang/listjenisbarang"><i class="fa fa-circle-o text-red"></i><span>list jenis barang</span></a></li>
            <li><a href="<?=base_url();?>supplier/listsupplier"><i class="fa fa-circle-o text-red"></i><span>list supplier</span></a></li>
      </ul>
   </li>
    <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Transaksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
         <li><a href="<?=base_url();?>pembelian/listpembelian"><i class="fa fa-circle-o text-yellow"></i><span>list pembelian</span></a></li>
         <li><a href="<?=base_url();?>penjualan/listpenjualan"><i class="fa fa-circle-o text-yellow"></i><span>list penjualan</span></a></li>
         
      </ul>
   </li>
    <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i> <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
        <li><a href="<?=base_url();?>pembelian/report"><i class="fa fa-circle-o text-aqua"></i><span>report pembelian</span></a></li>
        <li><a href="<?=base_url();?>penjualan/report1"><i class="fa fa-circle-o text-aqua"></i><span>report penjualan</span></a></li>
         
      </ul>
   </li>
    <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i> <span>logout</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
        <li><a href="<?=base_url();?>auth/logout"><i class="fa fa-circle-o text-orange"></i><span>logout</span></a></li>
       
         
      </ul>
   </li>

           
        <li><a href="https://Admin_LTE.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
