
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>toko jaya abadi | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?=base_url();?>assets/Admin_LTE/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url();?>assets/Admin_LTE/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url();?>assets/Admin_LTE/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url();?>assets/Admin_LTE/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=base_url();?>assets/Admin_LTE/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page" style="height: 50%" background="assets/Admin_LTE/dist/img/background-1.png">
<div class="login-box" style="width: 36%">
  <div class="login-box-body" align="center">
     <h2><b><font face="Georgia, Times New Roman, Times, serif" size="6" color="blue">Admin Toko jaya abadi</font></b></h2>
      <p class="login-box-msg"><b><h4 align="center">Sign in to start your session</h4></b></p>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <form name="form1" method="post" action="<?=base_url();?>auth/login">
      <div class="form-group has-feedback">
         <label><h4><b>Username:</b></h4></label>
         <div class="row" >
            <div class="col-sm-12">
       <input type="username" class="form_control"  placeholder="Username" name="username" style="width: 95%" id="username"/>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
    </div>
  </div>
      
      <div class="form-group has-feedback">
         <label><h4><b>Password:</b></h4></label>
             <div class="row" >
            <div class="col-sm-12">
           <input type="password" class="form_control" placeholder="Password" name="password"  style="width: 95%" id="password"/>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
    </div>
  </div>
      <BR>
      <div class="row" >
       <div class="col-xs-6">
         <div class="btn-primary btn-block btn-flat">
          <button type="submit" class="btn btn-primary btn-block btn-flat" autofocus="link">Sign In</button>
        </div>
      </div>
      <div class="col-xs-6">
         <div class="btn-primary btn-block btn-flat">
          <button type="submit" class="btn btn-primary btn-block btn-flat" autofocus="link">Cancel</button>
        </div>
    </div>
      </div>    
        <br>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>

       <!-- /.col -->
       
        <!-- /.col -->
      </div>
    </form>
    <br>
    <div class="social-auth-links text-center">
      <p>- OR -</p>
     
    </div>
    <br>
    <!-- /.social-auth-links -->
      <div class="row" >
       <div class="col-xs-6">
         <div class="password">
    <a href="#">I forgot my password</a>
    </div>
  </div>
   <div class="col-xs-6">
         <div class="register">
     <a href="register.html" class="text-center">Register a new membership</a>
    </div>
  </div>
</div>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?=base_url();?>assets/Admin_LTE/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url();?>assets/Admin_LTE/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?=base_url();?>assets/Admin_LTE/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
<?php
  if ($this->session->flashdata('info') == true) {
    echo $this->session->flashdata('info');
  }
    ?>
</body>
</html>
