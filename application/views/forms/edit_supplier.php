    <?php

foreach ($detail_supplier as $data) {
	$kode_supplier  = $data->kode_supplier;
	$nama_supplier  = $data->nama_supplier;
	$alamat  = $data->alamat;
	$telp  = $data->telp;
	
}

	?>
	
  <div style="color: red" align="center"><?= validation_errors(); ?></div>
<form action="<?=base_url()?>supplier/editsupplier/<?= $kode_supplier; ?>" method="POST">

    <div class="box box-info">
    <div class="box-header with-border">
    <h4 align="center"><b>Edit supplier</b></h4>
    </div class="col-md-3">
    </div>      
  <form class="form-horizontal">
    <div class="box-body">
    <div class="form-group">
      <label>kode supplier:</label>
    
     <input value="<?= $kode_supplier; ?>" type="text" name="kode_supplier" class="form-control" id="kode_supplier">
 
  </div>
  
    <div class="form-group">
      <label>Nama supplier:</label>   
   
    <input value="<?= $nama_supplier; ?>" type="text" name="nama_supplier" class="form-control" id="nama_supplier" maxlength="10">
  
   </div>

     <div class="form-group">
      <label>Alamat:</label>   
   
   <textarea name="alamat" id="alamat" cols="45" rows="3" class="form-control"><?= $alamat; ?>"</textarea>
  
   </div>
    <div class="form-group">
      <label>Telepon:</label>   
   
   <input value="<?= $telp; ?>" type="text" name="telp" class="form-control" id="telp">
  
   </div>
      <br>
   <div class="row">
      <div class="col-xs-1">
      <div class="form-group">
        <div><input type="submit" class="btn btn-primary" name="simpan" class="form-control" id="simpan" value="simpan"></div>
     </div>
    </div> 
    
    <div class="col-xs-1">
   <div class="form-group">
    <div><input type="submit" name="batal" class="btn btn-primary" class="form-control" id="batal" value="reset">
    </div>
    </div>
  </div>    
 </div> 
         <div class="checkbox">
            <label>
            <input type="checkbox"><b>remember me</b>
            </label>
             </div>
          </div>

        <div class="box-body"> 
      <a href="<?=base_url();?>supplier/listsupplier"><input type="button" class="btn btn-primary" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" value="kembali ke menu sebelumnya"></a>
      </div>
</form>
</form>
</div>
