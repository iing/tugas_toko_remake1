    <?php
foreach ($detail_barang as $data) {
	$kode_barang  = $data->kode_barang;
	$nama_barang  = $data->nama_barang;
	$harga_barang  = $data->harga_barang;
	$kode_jenis  = $data->kode_jenis;
	$stok  = $data->stok;
	
}
	?>
	
  <div style="color: red" align="center"><?= validation_errors(); ?></div>
<form action="<?=base_url()?>barang/editbarang/<?= $kode_barang; ?>" method="POST">

    <div class="box box-info">
    <div class="box-header with-border">
    <h4 align="center"><b>Edit BARANG</b></h4>
    </div class="col-md-3">
      </div>    
  <form class="form-horizontal">
    <div class="box-body">
    <div class="form-group">
      <label>kode barang:</label>
    
    <input value="<?= $kode_barang; ?>" type="text" name="kode_barang" class="form-control" id="kode_barang">
 
  </div>
  
    <div class="form-group">
      <label>Nama barang:</label>   
   
      <input value="<?= $nama_barang; ?>" type="text" name="nama_barang" class="form-control" id="nama_barang" maxlength="16">
  
   </div>

      <div class="form-group">
      <label>Harga barang:</label>  
    
   
       <input value="<?= $harga_barang; ?>" type="text" name="harga_barang" class="form-control" id="harga_barang" maxlength="10">
   </div>
  
  
   <div class="form-group">
      <label>Nama jenis</label>            
    <select name="kode_jenis" id="kode_jenis" class="form-control">
    <?php foreach($data_jenis_barang as $data) {
     $select_jenis_barang = ($data->kode_jenis ==
        $kode_jenis) ? 'selected' : '';
    ?>
       <option value="<?= $data->kode_jenis;?>" <?=$select_jenis_barang; ?>>
       <?= $data->nama_jenis; ?></option>
       
      
      <?php }?>
      
    </select>
              </div>
   
 <div class="form-group">
      <label>Stok barang</label>   
  
    <input value="<?= $stok; ?>" type="text" name="stok" class="form-control" id="stok" maxlength="10">
   
  </div>
 
            <br>
   <div class="row">
      <div class="col-xs-1">
      <div class="form-group">
        <div><input type="submit" class="btn btn-primary" name="simpan" class="form-control" id="simpan" value="simpan"></div>
     </div>
    </div> 
    
    <div class="col-xs-1">
   <div class="form-group">
    <div><input type="submit" name="batal" class="btn btn-primary" class="form-control" id="batal" value="reset">
    </div>
    </div>
  </div>    
 </div> 
         <div class="checkbox">
            <label>
            <input type="checkbox"><b>remember me</b>
            </label>
             </div>
          </div>

        <div class="box-body"> 
      <a href="<?=base_url();?>barang/listbarang"><input type="button" class="btn btn-primary" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" value="kembali ke menu sebelumnya"></a>
      </div>
</form>
</form>
</div>
