<style>
table, th, td {
  border-collapse:collapse;
  border:1px solid #999;
  font-family:Tahoma, Geneva, sans-serif;
  font-size:15px;
  padding: 0px;
} 
.head {
  background: rgb(206,220,231); /* Old browsers */
  background: -moz-linear-gradient(top,  rgba(206,220,231,1) 0%, rgba(89,106,114,1) 100%); /* FF3.6+ */
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(206,220,231,1)), color-stop(100%,rgba(89,106,114,1))); /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* Chrome10+,Safari5.1+ */
  background: -o-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* Opera 11.10+ */
  background: -ms-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* IE10+ */
  background: linear-gradient(to bottom,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* W3C */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#cedce7', endColorstr='#596a72',GradientType=0 ); /* IE6-9 */

}
 
.head th {
  padding:15px;
  color:#333;
  text-shadow:1px 1px 0px #CCC;
  font-size:15px;
  text-align: center;

}
.satu {
  background-color:#CECECE;

}
  
.satuhover {
  background-color:#BADFFE;
  font-weight:bold;
  cursor:pointer;

}
</style>
<table width="100%" height="40" border="0" align="center">
<tr class="head">
 
  <td colspan="5" align="center" bgcolor="white"><b><h4><i>" Laporan Data Penjualan Dari TANGGAl"</b></h4></i>
  <h4><?php
  $tgl_awal=$this->input->post('tgl_awal');
    //$pisah=explode('/', $tgl_awal);
    //$array=array($pisah[2],$pisah[1],$pisah[0]);
    //$tgl_awal=implode('-', $array);
   echo $tgl_awal; ?> "S/D" <?php
    $tgl_akhir=$this->input->post('tgl_akhir');
    //$pisah=explode('/', $tgl_akhir);
    //$array=array($pisah[2],$pisah[1],$pisah[0]);
    //$tgl_akhir=implode('-', $array);
    echo $tgl_akhir; ?></h4>
  </td>
 <buttom></buttom><p>

 </p>
  </center>
  
  </tr>
  </table>
 <br>
 <div class="box-body">
    <table id="example1" class="table table-bordered table-striped">

  <span class="badge bg-black"><a href="<?=base_url();?>penjualan/cetak/<?= $tgl_awal; ?>/<?= $tgl_akhir; ?>"><h5>Cetak pdf</h5></a>
  </span>
   <right>
   <span class="badge bg-black"><a href="<?=base_url();?>penjualan/report1"> <h5>kembali</h5></a>
  </span>
</right>
  <br></br>

  <thead>
    <tr class="satu" align="center">
     <th>No</th>
    <th>ID Penjualan</th>
    <th>No Transaksi</th>
    <th>Tanggal</th>
    <th>Total barang</th>
    <th>Total qty</th>
    <th>Jumlah nominal penjualan</th>
  </tr>
  <?php
  $no = 0;

  $total_keseluruhan  = 0;
 
    foreach ($data_penjualan as $data) {
      $no ++;
?>

      </thead>
        <tbody>
    <tr class="satuhover" align="center">
    <td><?= $no; ?></td>
    <td><?= $data->id_jual_h; ?></td>
    <td><?= $data->no_transaksi; ?></td>
    <td><?= $data->tanggal; ?></td>
    <td><?= $data->kode_barang; ?></td>
    <td><?= $data->qty; ?></td>
      <td align="right">Rp. <?= number_format($data->total); ?> ,-</td>
  </tr>

   <?php
        // hitung total
        $total_keseluruhan += $data->total;
        } 
    ?>
     
    <tr class="satuhover" align="center">
    <td colspan="6" align="center"><b>TOTAL KESELURUHAN Penjualan</b></td>
    <td  align="center">Rp. <b><?= number_format($total_keseluruhan); ?></b></td>
    </tr>

 </tbody>
        <tfoot>
                
          </tfoot>
           </div>
        </table>