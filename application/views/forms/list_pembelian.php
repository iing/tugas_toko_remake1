<style>
.center {
  text-align: center;
}

.pagination {
  display: inline-block;
}

.pagination a {
  color: red;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
  transition: background-color .3s;
  border: 1px solid green;
  margin: 0 4px;
}

.pagination a.active {
  background-color: #4CAF50;
  color: white;
  border: 1px solid #4CAF50;
}
.pagination a:hover:not(.active) {background-color: black;}
table, th, td {
  border-collapse:collapse;
  border:1px solid #999;
  font-family:Tahoma, Geneva, sans-serif;
  font-size:15px;
  padding: 0px;
} 
.head {
  background: rgb(206,220,231); /* Old browsers */
  background: -moz-linear-gradient(top,  rgba(206,220,231,1) 0%, rgba(89,106,114,1) 100%); /* FF3.6+ */
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(206,220,231,1)), color-stop(100%,rgba(89,106,114,1))); /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* Chrome10+,Safari5.1+ */
  background: -o-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* Opera 11.10+ */
  background: -ms-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* IE10+ */
  background: linear-gradient(to bottom,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* W3C */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#cedce7', endColorstr='#596a72',GradientType=0 ); /* IE6-9 */

}
 
.head th {
  padding:15px;
  color:#333;
  text-shadow:1px 1px 0px #CCC;
  font-size:15px;
  text-align: center;

}
.satu {
  background-color:#CECECE;

}
  
.satuhover {
  background-color:#BADFFE;
  font-weight:bold;
  cursor:pointer;

}
</style>
<div class="box-body">
    <table class="table table-bordered">
 <tr class="head">
  <td align="center"><b><h4><i>"Data pembelian"</b></h4></i></td>
  </tr>
</div>
  </table>
  
 <div class="box-body">
    <table id="example1" class="table table-bordered table-striped">

  <span class="badge bg-black"><a href="<?=base_url();?>pembelian/input"><h5>[New input pembelian]</h5></a>
  </span>
  <br></br>
    <form action="<?=base_url()?>pembelian/listpembelian" method="POST">
   <div class="input-group" style="color:red "> 
              <input name="caridata" id="Cari data" class="form-control" placeholder='Search...'  type='text' autocomplete="off" style="background-color:azure;" style="font-weight:bold; "  />
               <span class="input-group-btn">
                <button type="submit" name="tombol_cari" id="search-btn" value="Cari data" class="btn btn-flat"><i class="fa fa-search" align="left"></i>
                </button>
              </span>
        </div>
         <br>
   <?php
  if ($this->session->flashdata('info') == true) {
    echo $this->session->flashdata('info');
  }
  ?>

  <thead>
    <tr class="head">
     <th>no</th>
    <th>nomer transaksi</th>
    <th>Kode_Supplier</th>
    <th>Tanggal</th>
    <th>aksi</th>
  </tr>
  <?php
  $no = 0;
  $data_posisi = $this->uri->segment(4);
  $no = $data_posisi;
  if (count($data_pembelian) > 0) {
    foreach ($data_pembelian as $data) {
      $no ++;
?>

      </thead>
        <tbody>
    <tr class="satuhover" align="center">
    <td><?= $no; ?></td>
    <td><?= $data->no_transaksi; ?></td>
    <td><?= $data->kode_supplier; ?></td>
    <td><?= $data->tgl; ?></td>
    
      <td><span class="badge bg-black"><a href="<?=base_url();?>pembelian/inputDetail/<?= $data->id_pembelian_h; ?>"> <h5><font color="white" size="3" style="font-family:Tahoma, Geneva, sans-serif;">Detail</font></h5></a></span>
   </td>
  
  </tr>

  <?php } ?>
    <tr class="satu">
  <td colspan="7" align="right"><b>Halaman:</b> <?= $this->pagination->create_links();?></td>
  </tr>
  <?php
  } else {
    ?>
     <tr class="satuhover">
  <td colspan="7" align="center">--tidak ada data -------</td>
  </tr> 
  <?php } ?>
 </tbody>
        <tfoot>
                
          </tfoot>
        </table>
           
  </div>
</form>