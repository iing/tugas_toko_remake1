
  <div style="color: red" align="center"><?= validation_errors(); ?></div>
<form action="<?=base_url()?>karyawan/inputkaryawan" method="POST" enctype="multipart/form-data">

    <div class="box box-info">
    <div class="box-header with-border">
    <h4 align="center"><b>INPUT KARYAWAN</b></h4>
    </div class="col-md-3">
     </div>     
  <form class="form-horizontal">
    <div class="box-body">
    <div class="form-group">
      <label>NIK</label>
    
 <input type="text" name="nik" id="nik"  maxlength="10" class="form-control"  value="<?=$nik_baru?>">
 
  </div>
  
    <div class="form-group">
      <label>Nama karyawan</label>   
   
      <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control"  value="<?=set_value('nama_lengkap');?>" maxlength="50">
  
   </div>

      <div class="form-group">
      <label>tempat lahir</label>  
    
   
     <input type="text" name="tempat_lahir" id="tempat_lahir" value="<?=set_value('tempat_lahir');?>" maxlength="50" class="form-control">
   </div>
  
  
   <div class="form-group">
      <label>jenis kelamin</label>            
    <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
       <option value="Laki-laki">Laki-laki</option>
       <option value="Perempuan">Perempuan</option>
     </select>
      </div>
   
   <div class="form-group">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <label>tanggal lahir</label>
   <div class="input-group date">
    <div class="input-group-addon">
    <i class="fa fa-calendar"></i>

    </div>
    <script>
  $( function() {
    $( "#tgl_lahir" ).datepicker({dateFormat: "yy-mm-dd"});
   
  } );
  </script>
    <input type="text" name="tgl_lahir"  class="form-control id="tgl_lahir" value="<?=set_value('tgl_lahir');?>">
    </div>
    </div>
   
 <div class="form-group">
    <label>telepon</label>   
    <input type="text" name="telp" id="telp" class="form-control"  value="<?=set_value('telp');?>">
  </div>

   <div class="form-group">
    <label>alamat</label>
    <textarea name="alamat" id="alamat" class="form-control" rows="2"><?=set_value('alamat');?>"</textarea>
    </div>

   <div class="form-group">
    <label>jabatan</label>   
    <select name="kode_jabatan" id="kode_jabatan" class="form-control">
    <?php foreach($data_jabatan as $data) {?>
       <option value="<?= $data->kode_jabatan;?>">
       <?= $data->nama_jabatan; ?></option>
      <?php }?>
    </select>
  </div>
   
  
   <div class="form-group">
          <label>Upload photo</label>
    <input type="file" name="image" id="image" />
    </div>
 
   <div class="row">
  <div class="col-xs-1">
   <div class="form-group">
    <input type="submit" class="btn btn-primary" name="simpan" class="form-control" id="simpan" value="simpan">
     </div>
    </div> 
    
    <div class="col-xs-1">
   <div class="form-group">
    <input type="submit" name="batal" class="btn btn-primary" class="form-control" id="batal" value="batal">
    </div>
  </div>    
 </div> 
       <div class="checkbox">
        <label>
        <input type="checkbox"><b>remember me</b>
        </label>
         </div>
        </div>

        <div class="box-body"> 
      <a href="<?=base_url();?>karyawan/listkaryawan"><input type="button" class="btn btn-primary" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" value="kembali ke menu sebelumnya"></a>
    </form>
  </div>
</form>
   </div>