<?php
foreach ($detail_karyawan as $data) {
	$nik  = $data->nik;
	$nama_lengkap  = $data->nama_lengkap;
	$tempat_lahir  = $data->tempat_lahir;
	$tgl_lahir  = $data->tgl_lahir;
	$jenis_kelamin  = $data->jenis_kelamin;
	$alamat  = $data->alamat;
	$telp  = $data->telp;
	$kode_jabatan  = $data->kode_jabatan;
	$photo  = $data->photo;
}
     //pisah tanggal bualn tahun
	 $thn_pisah = substr($tgl_lahir, 0, 4);
	 $bln_pisah = substr($tgl_lahir, 5, 2);
	 $tgl_pisah = substr($tgl_lahir, 8, 2);

	?>
 <div style="color: red" align="center"><?= validation_errors(); ?></div>
<form action="<?=base_url()?>karyawan/editkaryawan/<?= $nik; ?>" method="POST" enctype="multipart/form-data">

          <div class="box box-info">
            <div class="box-header with-border">
              <h4 align="center"><B>EDIT KARYAWAN</B></h4>
            </div class="col-md-3">
             </div>
     <form class="form-horizontal">
      <div class="box-body">

        <div class="form-group">
          <label>NIK</label>
          <div><input value="<?= $nik; ?>" type="text" class="form-control" name="nik" id="nik"></div>
          </div>

          <div class="form-group">
            <label>Nama karyawan</label>
            <div><input value="<?= $nama_lengkap; ?>" type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" maxlength="50"></div>
           </div>
      
        <div class="form-group">
        <label>Tempat lahir</label>
        <div><input value="<?= $tempat_lahir; ?>" type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" maxlength="50"></div>
          </div>

        <div class="form-group">
         <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
         <link rel="stylesheet" href="/resources/demos/style.css">
         <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
         <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <label>Tanggal lahir</label>
           <script>
  $( function() {
    $( "#tgl_lahir" ).datepicker({dateFormat: "yy-mm-dd"});
   
  } );
  </script>
         <input  value="<?=$tgl_lahir;?>" type="text" class="form-control" name="tgl_lahir" id="tgl_lahir">
           </div>

  <div class="form-group">
  <label>Jenis kelamin</label>
    <div><select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
       <?php
  if($jk == 'P'){
    $slc_p = 'selected';
    $slc_l = '';
  }else if($jk == 'L'){
    $slc_l = 'selected';
    $slc_p = '';
  }else{
    $slc_p = '';
    $slc_l = '';
    }
  ?>
     <option <?=$slc_p;?> value="p">perempuan</option>
      <option  <?=$slc_l;?> value="l">laki-laki</option> 
    </select>
  </div>
     </div>

    <div class="form-group">
      <label>Alamat</label>
      <div><textarea name="alamat" id="alamat" class="form-control" cols="20" rows="2"  >"<?= $alamat; ?>"</textarea></div>
      </div>

       <div class="form-group">
        <label>Telepon</label>
      <div><input value="<?= $telp; ?>" type="text" class="form-control" name="telp" id="telp"></div>
        </div>

    <div class="form-group">
      <label>Jabatan</label>
      <div><select name="kode_jabatan" id="kode_jabatan" class="form-control">
    <?php foreach($data_jabatan as $data) {
          $select_jabatan = ($data->kode_jabatan ==
        $kode_jabatan) ? 'selected' : '';
    ?>
       <option value="<?= $data->kode_jabatan;?>" <?=$select_jabatan; ?>>
       <?= $data->nama_jabatan; ?></option>
      <?php }?>
    </select>
  </div>
    </div>

        <div class="form-group">
      <label>Upload photo</label>
      <div><input type="file" name="image" id="image">
      <input type="hidden" name="foto_old" id="foto_old" value="<?= $photo; ?>">
    </div>
       </div>

       <div class="row">
      <div class="col-xs-1">
      <div class="form-group">
        <div><input type="submit" class="btn btn-primary" name="simpan" class="form-control" id="simpan" value="simpan"></div>
     </div>
    </div> 
    
    <div class="col-xs-1">
   <div class="form-group">
    <div><input type="submit" name="reset" class="btn btn-primary" class="form-control" id="reset" value="reset">
    </div>
    </div>
  </div>    
 </div> 

            <div class="checkbox">
            <label>
            <input type="checkbox"><b>remember me</b>
             </label>
            </div>
            </div>

        <div class="box-body">
         <a href="<?=base_url();?>karyawan/listkaryawan"><input type="button" class="btn btn-primary" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" value="kembali ke menu sebelumnya"></a>
              </div>
            </form>
          </form>
        </div>
          
          
        
