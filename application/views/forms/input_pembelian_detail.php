<style>
table, th, td {
  border-collapse:collapse;
  border:1px solid #999;
  font-family:Tahoma, Geneva, sans-serif;
  font-size:15px;
  padding: 0px;
} 
.head {
  background: rgb(206,220,231); /* Old browsers */
  background: -moz-linear-gradient(top,  rgba(206,220,231,1) 0%, rgba(89,106,114,1) 100%); /* FF3.6+ */
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(206,220,231,1)), color-stop(100%,rgba(89,106,114,1))); /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* Chrome10+,Safari5.1+ */
  background: -o-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* Opera 11.10+ */
  background: -ms-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* IE10+ */
  background: linear-gradient(to bottom,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* W3C */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#cedce7', endColorstr='#596a72',GradientType=0 ); /* IE6-9 */

}
.head th {
  padding:15px;
  color:#333;
  text-shadow:1px 1px 0px #CCC;
  font-size:15px;
  text-align: center;

}
.satu {
  background-color:#CECECE;

}  
.satuhover {
  background-color:#BADFFE;
  font-weight:bold;
  cursor:pointer;

}
</style>      
  <div style="color: red" align="center"><?= validation_errors(); ?></div>
 <form action="<?=base_url()?>pembelian/inputDetail/<?= $id_header; ?>" method="POST">
      <div class="box box-info">
    <div class="box-header with-border">
    <h4 align="center">input pembelian detail</h4>
    </div class="col-md-3">
</div>
          
  <form class="form-horizontal">
    <div class="box-body">
    <div class="form-group">
      <label>Nama Barang:</label>  
    <select id="kode_barang" name="kode_barang" class="form-control">
    <?php foreach($data_barang as $data) { ?>
    <option value="<?= $data->kode_barang; ?>"><?= $data->nama_barang; ?></option>
     <?php }?>
    </select>
    </div>
 
  <div class="form-group">
      <label>Qty:</label>  
    <input type="text" id="qty" name="qty" class="form-control" value="<?=set_value('qty');?>">
  </div>

  <div class="form-group">
      <label>Harga Barang:</label>  
     <input type="text" id="harga" name="harga" class="form-control" value="<?=set_value('harga');?>">
  </div>
  <BR>

   <div class="row" >
    <div class="col-xs-1">
   <div class="form-group">
     <button type="submit" name="hitung" class="btn btn-primary" value="hitung">hitung</button>
 </div>
 </div> 

 <div class="col-xs-1">
 <div class="form-group">
  <a href="<?= base_url(); ?>pembelian/index"><input type="button" class="btn btn-primary" name="kembali ke menu" id="kembali ke menu" value="kembali ke menu"></a>
 </div> 
 </div>           
</div>
</form>
</form>
</div>

<div class="box-body">
    <table id="example1" class="table table-bordered table-striped">
	<thead>
    <tr class="head">
		<th>No</th>
		<th>Kode Barang</th>
		<th>Nama Barang</th>
		<th>Qty</th>
		<th>Harga</th>
		<th>Jumlah</th>
    </tr>

    <?php
        $no = 0;
        $total_hitung = 0;
        foreach ($data_pembelian_detail as $data) {
            $no++;
    ?>
	</thead>
    <tbody>
    <tr class="satuhover" align="center">
		<td><?= $no; ?></td>
		<td><?= $data->kode_barang; ?></td>
		<td><?= $data->nama_barang; ?></td>
		<td><?= $data->qty; ?></td>
		<td>Rp. <?= number_format($data->harga); ?> ,-</td>
		<td>Rp. <?= number_format($data->jumlah); ?> ,-</td>
    </tr>
    <?php
        // hitung total
        $total_hitung += $data->jumlah;
        } 
    ?>
    <tr  class="satuhover" align="center">
		<td colspan="5" align="center"><b>TOTAL PEMBELIAN</b></td>
		<td  align="center">Rp. <b><?= number_format($total_hitung); ?></b></td>
    </tr>    
</tbody>
    <tfoot>      
          </tfoot>
           </div>

        </table>     
 