
 <form id="forms" method="POST" action="<?=base_url();?>penjualan/report_penjualan">
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $( "#tgl_awal" ).datepicker({dateFormat: "yy-mm-dd"});
    $( "#tgl_akhir" ).datepicker({dateFormat: "yy-mm-dd"});
  } );
  </script>
 <div class="box box-primary" align="center">
         <div class="box-header with-border">
          <h3 class="box-title">report penjualan</h3>
            </div>
            </div>

            <form role="form">
              <div class="box-body">

    <div class="form-group">
    <label>Date First:</label>
    <div class="input-group date">
    <div class="input-group-addon">
    <i class="fa fa-calendar"></i> 
    </div> 
   <div><input type="text" name="tgl_awal" class="form-control" id="tgl_awal"></div><br>
   </div>
</div>
<br>
<div class="form-group">
    <label>Date Last:</label>
    <div class="input-group date">
    <div class="input-group-addon">
    <i class="fa fa-calendar"></i> 
    </div>   
<div><input type="text" name="tgl_akhir" class="form-control" id="tgl_akhir"></div>
 </div>
</div>
<br></br>

<div class="row">
  <div class="col-sm-12">
   <div class="form-group" align="center">
<input type="submit" value="proses" class="btn btn-primary" style="width: 30%" style="height:30%" name="proses" id="proses">
</div>
</div>
</div>
<script>
$(document).ready(function(){
  $('#proses').on('click', function(event) {
    event.preventDefault();
    var tgl_awal = $('#tgl_awal').val();
    var tgl_akhir = $('#tgl_akhir').val();
    if (tgl_awal == '' || tgl_akhir == '') {
      alert('tanggal tidak boleh kosong');
    }else if (new Date(tgl_awal) > new Date(tgl_akhir)) {
      alert('format waktu salah input');
    } else {
      $('#forms').submit();
    }
  });
  
});
</script>
</form>
</div>
</form>   
</div>  