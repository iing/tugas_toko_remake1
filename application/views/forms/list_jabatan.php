<style>
.center {
  text-align: center;
}

.pagination {
  display: inline-block;
}

.pagination a {
  color: red;
  float: left;
  padding: 3px 12px;
  text-decoration: none;
  transition: background-color .3s;
  border: 1px solid green;
  margin: 0 4px;
}

.pagination a.active {
  background-color: #4CAF50;
  color: white;
  border: 1px solid #4CAF50;
}
.pagination a:hover:not(.active) {background-color: black;}
 table, th, td {
  border-collapse:collapse;
  border:1px solid #999;
  font-family:Tahoma, Geneva, sans-serif;
  font-size:15px;
  padding: 0px;
}

 
.head {
  background: rgb(206,220,231); /* Old browsers */
  background: -moz-linear-gradient(top,  rgba(206,220,231,1) 0%, rgba(89,106,114,1) 100%); /* FF3.6+ */
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(206,220,231,1)), color-stop(100%,rgba(89,106,114,1))); /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* Chrome10+,Safari5.1+ */
  background: -o-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* Opera 11.10+ */
  background: -ms-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* IE10+ */
  background: linear-gradient(to bottom,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* W3C */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#cedce7', endColorstr='#596a72',GradientType=0 ); /* IE6-9 */

}
 
.head th {
  padding:15px;
  color:green;
  text-shadow:1px 1px 0px #CCC;
  font-size:15px;
  text-align: center;
   font-weight:bold;

}
.satu {
  background-color:#CECECE;

}
 
 
.satuhover {
  background-color:#BADFFE;
  font-weight:bold;
  cursor:pointer;

}

</style>
<div class="box-body">
    <table class="table table-bordered">
 <tr class="head">
  <td align="center"><b><h4><i>"Data jabatan"</b></h4></i></td>
  </tr>
</div>
  </table>
 
<div class="box-body">
    <table id="example1" class="table table-bordered table-striped">
      
      <span class="badge bg-black"><a href="<?=base_url();?>jabatan/inputjabatan"><h5>[New input jabatan]</h5></a></span>
        <br></br>
        <form action="<?=base_url()?>jabatan/listjabatan" method="POST" class="sidebar-form">
        <div class="input-group" style="color:red "> 
              <input name="caridata" id="Cari data" class="form-control" placeholder='Search...'  type='text' autocomplete="off" style="background-color:azure;" style="font-weight:bold; "  />
               <span class="input-group-btn">
                <button type="submit" name="tombol_cari" id="search-btn" value="Cari data" class="btn btn-flat"><i class="fa fa-search" align="left"></i>
                </button>
              </span>
        </div>
         <br>

    <?php
  if ($this->session->flashdata('info') == true) {
    echo $this->session->flashdata('info');
  }
  ?>
        <thead>
          <tr class="head">
            <th>no</th>
            <th>kode jabatan</th>
            <th>Nama</th>
            <th>keterangan</th>
            <th>aksi</th>
             </tr>

      <?php
$no = 0;
  $data_posisi = $this->uri->segment(4);
  $no = $data_posisi;
  if (count($data_jabatan) > 0) {
    foreach ($data_jabatan as $data) {
   $no ++;
?>
        </thead>
        <tbody>

          <tr class="satuhover" align="center">
              <td><?= $no; ?></td>
              <td><?= $data->kode_jabatan; ?></td>
              <td><?= $data->nama_jabatan; ?></td>
              <td><?= $data->keterangan; ?></td>
              <td><span class="badge bg-brown"><a href="<?=base_url();?>jabatan/detailjabatan/<?= $data->kode_jabatan; ?>"><h5><font color="white" size="3">Detail</font></h5></a></span>
              <span class="badge bg-green"><a href="<?=base_url();?>jabatan/editjabatan/<?= $data->kode_jabatan; ?>"><h5><font color="red" size="3">Edit</font></h5></a></span>
              <span class="badge bg-red"><a href="<?=base_url();?>jabatan/deletejabatan/<?= $data->kode_jabatan; ?>" onClick="return confirm('Yakin ingin hapus data?');"><h5><font color="black" size="3">delete</font></h5></a></span> 
   </td>
     </tr>

       <?php } ?>
   <tr class="satuhover">
  <td colspan="5" align="right"><b>Halaman:</b> <?= $this->pagination->create_links();?></td>
  </tr>
  <?php
  } else {
    ?>
     <tr align="center" class="satuhover">
  <td colspan="7">--tidak ada data -------</td>
  </tr> 
  <?php } ?>

                
      </tbody>
        <tfoot>
                
          </tfoot>
        </table>
           
  </div>
 </form>
  