
  <div style="color: red" align="center"><?= validation_errors(); ?></div>
<form action="<?=base_url()?>barang/inputbarang" method="POST">

    <div class="box box-info">
    <div class="box-header with-border">
    <h4 align="center"><b>INPUT BARANG</b></h4>
    </div class="col-md-3">
     </div>     
  <form class="form-horizontal">
    <div class="box-body">
    <div class="form-group">
      <label>kode barang:</label>
    
  <input type="text" name="kode_barang" id="kode_barang" class="form-control"  maxlength="10" value="<?=$kode_barang_baru?>">
 
  </div>
  
    <div class="form-group">
      <label>Nama barang:</label>   
   
      <input type="text" name="nama_barang" id="nama_barang" value="<?= set_value('nama_barang');?>" maxlength="10" class="form-control">
  
   </div>

      <div class="form-group">
      <label>Harga barang:</label>  
    
   
      <input type="text" name="harga_barang" id="harga_barang" value="<?= set_value('harga_barang');?>" maxlength="10" class="form-control">
   </div>
  
  
   <div class="form-group">
      <label>Nama jenis:</label>            
     <select name="kode_jenis" id="kode_jenis" class="form-control">
    <?php foreach($data_jenis_barang as $data) {?>
       <option value="<?= $data->kode_jenis;?>">
       <?= $data->nama_jenis; ?></option>
      <?php }?> 
    </select>
              </div>

 <div class="form-group">
      <label>Stok barang:</label>   
  
     <input type="text" name="stok" id="stok" maxlength="10" class="form-control" value="<?=set_value('stok');?>">
   
  </div>
 
        <br>
   <div class="row">
      <div class="col-xs-1">
      <div class="form-group">
        <div><input type="submit" class="btn btn-primary" name="simpan" class="form-control" id="simpan" value="simpan"></div>
     </div>
    </div> 
    
    <div class="col-xs-1">
   <div class="form-group">
    <div><input type="submit" name="batal" class="btn btn-primary" class="form-control" id="batal" value="batal">
    </div>
    </div>
  </div>    
 </div> 
         <div class="checkbox">
            <label>
            <input type="checkbox"><b>remember me</b>
            </label>
             </div>
          </div>

        <div class="box-body"> 
      <a href="<?=base_url();?>barang/listbarang"><input type="button" class="btn btn-primary" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" value="kembali ke menu sebelumnya"></a>
      </div>
</form>
</form>
</div>
