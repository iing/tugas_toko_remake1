
 <form id="forms" method="POST" action="<?=base_url();?>pembelian/report_pembelian">
<script>
  $( function() {
    $( "#tgl_awal" ).datepicker({dateFormat: "yy-mm-dd"});
    $( "#tgl_akhir" ).datepicker({dateFormat: "yy-mm-dd"});
  } );
  </script> 
        <div class="box box-primary" align="center">
         <div class="box-header with-border">
          <h3 class="box-title">report pembelian</h3>
            </div>
            </div>

            <form role="form">
              <div class="box-body">

    <div class="form-group">
    <label>Tanggal mulai:</label>
    <div class="input-group date">
    <div class="input-group-addon">
    <i class="fa fa-calendar"></i> 
    </div> 
   <div><input type="text" name="tgl_awal" class="form-control" id="tgl_awal"></div><br>
   </div>
</div>
<br>
<div class="form-group">
    <label>Tanggal akhir:</label>
    <div class="input-group date">
    <div class="input-group-addon">
    <i class="fa fa-calendar"></i> 
    </div>   
<div><input type="text" name="tgl_akhir" class="form-control" id="tgl_akhir"></div>
 </div>
</div>
<br></br>

<div class="row">
  <div class="col-sm-12">
   <div class="form-group" align="center">
<input type="submit" value="proses" class="btn btn-primary" style="width: 30%" style="height:30%" name="proses" id="proses">
</div>
</div>
</div>

<script>
$(document).ready(function(){
	$('#proses').on('click', function(event) {
		event.preventDefault();
		var tgl_awal = $('#tgl_awal').val();
		var tgl_akhir = $('#tgl_akhir').val();
		if (tgl_awal == '' || tgl_akhir == '') {
			alert('tanggal tidak boleh kosong');
		}else if (new Date(tgl_awal) > new Date(tgl_akhir)) {
			alert('format waktu salah input');
		} else {
			$('#forms').submit();
		}
	});
	
});
</script>
</form>
</div>
</form>		
</div>	