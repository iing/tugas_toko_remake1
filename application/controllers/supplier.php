<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Supplier extends CI_controller {

	
	
	
	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("supplier_model");
		
		// cek login akses
		$user_login = $this->session->userdata();
		if (count($user_login) <= 1) {
			redirect("auth/index", "refresh");
	}
	}
	
	public function index()
	
	{
		$this->listsupplier();
	}
	
	public function listsupplier()
	
	{
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian_supplier', $data['kata_pencarian']);
		} else {
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian_supplier');
		}
		//$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		$data['data_supplier'] = $this->supplier_model->tombolpagination($data['kata_pencarian']);
		$data['content'] = 'forms/list_supplier';
		$this->load->view('home', $data);
	}
	public function inputsupplier()
	
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		$data['kode_supplier_baru'] = $this->supplier_model->createKodeUrut();
		//if (!empty($_REQUEST)) {
			//$m_supplier = $this->supplier_model;
			//$m_supplier->save();
			//redirect("supplier/index", "refresh");
		//}
		$validation = $this->form_validation;
		$validation->set_rules($this->supplier_model->rulesinput());
		
		if ($validation->run()) {
			$this->supplier_model->save();
			$this->session->set_flashdata('info', '<div style="color : green">simpan data berhasil !</div>');
			redirect("supplier/index", "refresh");
		}
			
		//$this->load->view('input_supplier');
		$data['content'] = 'forms/input_supplier';
		$this->load->view('home', $data);
	}
	public function detailsupplier($kode_supplier)
	
	{
		$data['detail_supplier'] = $this->supplier_model->detail($kode_supplier);
		//$this->load->view('detail_supplier', $data);
		$data['content'] = 'forms/detail_supplier';
		$this->load->view('home', $data);
	}
	public function editsupplier($kode_supplier)
	
	{
		$data['detail_supplier'] = $this->supplier_model->detail($kode_supplier);
		//if (!empty($_REQUEST)) {
			//$m_supplier = $this->supplier_model;
			//$m_supplier->update($kode_supplier);
			//redirect("supplier/index", "refresh");
		//}
		$validation = $this->form_validation;
		$validation->set_rules($this->supplier_model->rulesedit());
		
		if ($validation->run()) {
			$this->supplier_model->update($kode_supplier);
			$this->session->set_flashdata('info', '<div style="color : green">UPDATE data berhasil !</div>');
			redirect("supplier/index", "refresh");
			
		}
			
		//$this->load->view('edit_supplier', $data);
		$data['content'] = 'forms/edit_supplier';
		$this->load->view('home', $data);
	}
	public function deletesupplier($kode_supplier)
	
	{
		
		
			$m_supplier = $this->supplier_model;
			$m_supplier->delete($kode_supplier);
			redirect("supplier/index", "refresh");
		}
	
}
