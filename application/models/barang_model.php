<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class barang_model extends CI_Model
{
	//panggil nama table
	private $_table = "barang";
	
	
	
	public function tampilDataBarang()
	
	{
		//seperti : select * from <name_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function tampilDataBarang2()
	
	{
		$this->db->select('jenis_barang.nama_jenis');
            $this->db->from('barang');
            $this->db->join('jenis_barang', 'jenis_barang.kode_jenis = barang.kode_barang');
            
             $query = $this->db->get();  
          return $query->result();
	}
	
	public function tampilDataBarang3()
	
	{
		$this->db->select('*');
		$this->db->order_by('kode_barang', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	
	{
		$kode_barang = $this->input->post('kode_barang');
		$sql = $this->db->query("SELECT kode_barang from barang where kode_barang='$kode_barang'");
		$cek_kode_barang = $sql->num_rows();
		if ($cek_kode_barang > 0)
		{
			$this->session->set_flashdata();
			redirect('barang/inputbarang');
		}else{
			$kode_barang = $this->input->post('kode_barang');
		}
			
		$data['kode_barang'] =$this->input->post('kode_barang');
		$data['nama_barang'] =$this->input->post('nama_barang');
		$data['harga_barang'] =$this->input->post('harga_barang');
		$data['kode_jenis'] =$this->input->post('kode_jenis');
		$data['stok'] =$this->input->post('stok');
		
		$data['flag'] =1;
		$this->db->insert($this->_table, $data);

	}
	public function detail($kode_barang)
	
	{
		$this->db->select('*');
		$this->db->where('kode_barang', $kode_barang);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function update($kode_barang)
	
	{
		$kode_barang = $this->input->post('kode_barang');
		$sql = $this->db->query("SELECT kode_barang from barang where kode_barang='$kode_barang'");
		$cek_kode_barang = $sql->num_rows();
		if ($cek_kode_barang > 0)
		{
			$this->session->set_flashdata();
			redirect('barang/inputbarang');
		}else{
			$kode_barang = $this->input->post('kode_barang');
		}
		
		$data['nama_barang'] =$this->input->post('nama_barang');
		$data['harga_barang'] =$this->input->post('harga_barang');
		$data['kode_jenis'] =$this->input->post('kode_jenis');
		$data['stok'] =$this->input->post('stok');
		
		$data['flag'] =1;
		
		$this->db->where('kode_barang', $kode_barang);
     	$this->db->update($this->_table, $data);

	}
	public function delete($kode_barang)
	
	{
		
		$this->db->where('kode_barang', $kode_barang);
		
		 $this->db->delete($this->_table);
		
	}
	
	public function rules()
	
	{
		return [
		[
		'field' => 'kode_barang',
		'label' => 'kode barang',
		'rules' => 'required|max_length[5]',
		'errors' => [
		   'required' => 'kode barang tidak boleh kosong.',
		   'max_length' => 'kode barang tidak boleh lebih dari 5 karakter.',
		   ],
		   ],
		   [
		 'field' => 'nama_barang',
		 'label' => 'nama barang',
		 'rules' => 'required',
		 'errors' => [
		   'required' => 'nama barang tidak boleh kosong.',
           		   ],
		   ],
		   [
         'field' => 'harga_barang',
		'label' => 'harga barang',
		'rules' => 'required|numeric',
		'errors' => [
		   'required' => 'harga barang tidak boleh kosong.',
		   'numeric' => 'harga barang harus angka.',

           		   ],
		   ],
		   [
		'field' => 'kode_jenis',
		'label' => 'kode jenis',
		'rules' => 'required',
		'errors' => [
		   'required' => 'kode jenis tidak boleh kosong.',

           		   ],
		   ],
		   [
		 'field' => 'stok',
		'label' => 'stok barang',
		'rules' => 'required|numeric',
		'errors' => [
		   'required' => 'stok barang tidak boleh kosong.',
		   'numeric' => 'stok barang harus angka.',

           		   ],
		   ]
		   ];
		   


	}

	public function rulesinput()
	
	{
		return [
		[
		'field' => 'kode_barang',
		'label' => 'kode barang',
		'rules' => 'required|max_length[5]|is_unique[barang.kode_barang]',
		'errors' => [
		   'required' => 'kode barang tidak boleh sama.',
		   'max_length' => 'kode barang tidak boleh lebih dari 5 karakter.',
		   'is_unique' => 'kode barang tidak boleh sama.',
		   ],
		   ],
		   [
		 'field' => 'nama_barang',
		 'label' => 'nama barang',
		 'rules' => 'required',
		 'errors' => [
		   'required' => 'nama barang tidak boleh kosong.',
           		   ],
		   ],
		   [
         'field' => 'harga_barang',
		'label' => 'harga barang',
		'rules' => 'required|numeric',
		'errors' => [
		   'required' => 'harga barang tidak boleh kosong.',
		   'numeric' => 'harga barang harus angka.',

           		   ],
		   ],
		   [
		'field' => 'kode_jenis',
		'label' => 'kode jenis',
		'rules' => 'required',
		'errors' => [
		   'required' => 'kode jenis tidak boleh kosong.',

           		   ],
		   ],
		   [
		 'field' => 'stok',
		'label' => 'stok barang',
		'rules' => 'required|numeric',
		'errors' => [
		   'required' => 'stok barang tidak boleh kosong.',
		   'numeric' => 'stok barang harus angka.',

           		   ],
		   ]
		   ];
		   


	}

	public function rulesedit()
	
	{
		return [
		[
		'field' => 'kode_barang',
		'label' => 'kode barang',
		'rules' => 'required|max_length[5]|is_unique[barang.kode_barang]',
		'errors' => [
		   'required' => 'kode barang tidak boleh sama.',
		   'max_length' => 'kode barang tidak boleh lebih dari 5 karakter.',
		   'is_unique' => 'kode barang tidak boleh sama.',
		   ],
		   ],
		   [
		 'field' => 'nama_barang',
		 'label' => 'nama barang',
		 'rules' => 'required',
		 'errors' => [
		   'required' => 'nama barang tidak boleh kosong.',
           		   ],
		   ],
		   [
         'field' => 'harga_barang',
		'label' => 'harga barang',
		'rules' => 'required|numeric',
		'errors' => [
		   'required' => 'harga barang tidak boleh kosong.',
		   'numeric' => 'harga barang harus angka.',

           		   ],
		   ],
		   [
		'field' => 'kode_jenis',
		'label' => 'kode jenis',
		'rules' => 'required',
		'errors' => [
		   'required' => 'kode jenis tidak boleh kosong.',

           		   ],
		   ],
		   [
		 'field' => 'stok',
		'label' => 'stok barang',
		'rules' => 'required|numeric',
		'errors' => [
		   'required' => 'stok barang tidak boleh kosong.',
		   'numeric' => 'stok barang harus angka.',

           		   ],
		   ]
		   ];
		   


	}
	public function tampilDataBarang2pagination($perpage, $uri, $data_pencarian)
	{
		$this->db->select('barang.kode_barang,barang.nama_barang,barang.harga_barang,barang.stok,barang.kode_jenis,jenis_barang.kode_jenis,jenis_barang.nama_jenis');
		$this->db->join('jenis_barang','jenis_barang.kode_jenis=barang.kode_jenis');
		if (!empty($data_pencarian)) {
			$this->db->like('nama_barang', $data_pencarian);
		}
		$this->db->order_by('kode_barang', 'asc');
		
		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return $get_data->result();
		} else {
			return null;
		}
		
		}
		
	
	public function tombolpagination($data_pencarian)
	{
		$this->db->like('nama_barang', $data_pencarian);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();
		
		$pagination['base_url'] = base_url(). 'barang/listbarang/load/';
		$pagination['total_rows'] = $hasil;
		$pagination['per_page'] = "2";
		$pagination['uri_segment'] = 4;
		$pagination['num_links'] = 2;
		
		// custom paging configuration
		$pagination['full_tag_open'] = '<div class="pagination">';
		$pagination['full_tag_close'] = '</div>';
		
		$pagination['first_link'] = 'first page';
		$pagination['first_tag_open'] = '<span class="firstlink">';
		$pagination['first_tag_close'] = '</span>';
		
		$pagination['last_link'] = 'last page';
		$pagination['last_tag_open'] = '<span class="lastlink">';
		$pagination['last_tag_close'] = '</span>';
		
		$pagination['next_link'] = 'next page';
		$pagination['next_tag_open'] = '<span class="nextlink">';
		$pagination['next_tag_close'] = '</span>';
		
		$pagination['prev_link'] = 'prev page';
		$pagination['prev_tag_open'] = '<span class="prevlink">';
		$pagination['prev_tag_close'] = '</span>';
		
	
		$pagination['cur_tag_open'] = '<span class="curlink">';
		$pagination['cur_tag_close'] = '</span>';
		
		$pagination['num_tag_open'] = '<span class="numlink">';
		$pagination['num_tag_close'] = '</span>';
		
		$this->pagination->initialize($pagination);
		$hasil_pagination = $this->tampilDataBarang2pagination($pagination['per_page'],
		$this->uri->segment(4), $data_pencarian);
		
		return $hasil_pagination;
	}
	public function createKodeUrut() {
		$this->db->select('MAX(kode_barang) as kode_barang');
		$query = $this->db->get($this->_table);
		$result = $query->row_array();
		
		$kode_barang_terakhir = $result['kode_barang'];
		
		$label = "BR";
		$no_urut_lama = (int) substr($kode_barang_terakhir, 2, 3);
		$no_urut_lama ++;
		
		$no_urut_baru = sprintf("%03s", $no_urut_lama);
		$kode_barang_baru = $label . $no_urut_baru;
		
		return $kode_barang_baru;
	}
	
	public function TampilHargaBarang($kode_barang)
    {
        $query  = $this->db->query("SELECT * FROM " . $this->_table . " 
        WHERE flag = 1 AND kode_barang = '$kode_barang'");
        $hasil = $query->result();

        foreach ($hasil as $data){
            $harga = $data->harga_barang;
        }
        
        return $harga;
    }

    public function updatestok($kode_barang, $qty)
    {
        $cari_stok  = $this->detail($kode_barang);
       
        foreach ($cari_stok as $data){
            $stok = $data->stok;
        }
        $jumlah_stok = $stok + $qty;
        $data_barang['stok'] = $jumlah_stok;
        
        $this->db->where('kode_barang', $kode_barang);
        $this->db->update('barang', $data_barang);
    }

     public function updatestok1($kode_barang, $qty)
    {
        $cari_stok  = $this->detail($kode_barang);
       
        foreach ($cari_stok as $data){
            $stok = $data->stok;
        }
        $jumlah_stok = $stok - $qty;
        $data_barang['stok'] = $jumlah_stok;
        
        $this->db->where('kode_barang', $kode_barang);
          $this->db->update('barang', $data_barang);
    }
	
}
