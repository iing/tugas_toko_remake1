-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2019 at 06:43 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_jaya_abadi`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(5) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `harga_barang` float NOT NULL,
  `kode_jenis` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `harga_barang`, `kode_jenis`, `flag`, `stok`) VALUES
('BR003', 'buku', 360000, 'JN004', 1, 2),
('BR004', 'kipas', 1000000, 'JN004', 1, 4),
('BR005', 'bantal', 800000, 'JN004', 1, 2),
('BR006', 'hp', 10000000, 'JN001', 1, 6),
('BR007', 'jam', 560000, 'JN001', 1, 0),
('BR008', 'hp asus', 4000, 'JN001', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `kode_jabatan` varchar(5) NOT NULL,
  `nama_jabatan` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`kode_jabatan`, `nama_jabatan`, `keterangan`, `flag`) VALUES
('JB001', 'Direktur', 'Ob', 1),
('JB002', 'Admin', 'Operasional', 1),
('JB003', 'admin', 'Opersional', 1),
('JB004', 'Manager', 'Admin', 1),
('JB005', 'ob', 'ob', 1),
('JB006', 'marketing', 'sales marketing', 1),
('JB007', 'hrd', 'hrd', 1),
('JB008', 'polisi', 'polisi', 1),
('JB009', 'pilot', 'pilot', 1),
('JB010', 'dosen', 'dosen', 1),
('JB012', 'admin', 'ob', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `kode_jenis` varchar(5) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_barang`
--

INSERT INTO `jenis_barang` (`kode_jenis`, `nama_jenis`, `flag`) VALUES
('JN001', 'Alat Tulis kantor', 1),
('JN002', 'Perangkat keras', 1),
('JN003', 'alat tulis', 1),
('JN004', 'alat rumah', 1),
('JN005', 'pensil', 1),
('JN006', 'perangkat', 1),
('JN007', 'alat rumah', 1);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `nik` varchar(10) NOT NULL,
  `nama_lengkap` varchar(150) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `kode_jabatan` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `photo` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`nik`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `alamat`, `telp`, `kode_jabatan`, `flag`, `photo`) VALUES
('122222222', 'dafu', 'cirebon', '1970-09-13', 'p', '"""jl.medan selatan no.87"""', '0213456774', 'JB009', 1, '20190405_122222222.jpg'),
('1233444', 'iyo', 'surabaya', '1959-01-01', 'l', '"jl.pojok baru"', '134555522', 'JB004', 1, '20190405_1233444.jpg'),
('1555555', 'arqin', 'jakarta', '1959-01-01', 'p', 'jl.tipar cakung no.87', '099999999', 'JB002', 1, '20190403_1555555.jpg'),
('19024', 'rei', 'cirebon', '2019-04-17', 'L', 'jl.merdeka', '08776655555', 'JB007', 1, 'default.jpg'),
('19035557', 'yaya', 'bandung', '2019-04-11', 'L', 'jl.kesemek""""', '0488888', 'JB001', 1, 'default.jpg'),
('1904043', 'rere', 'bandung', '2019-04-20', 'P', 'jl.merdeka', '08776655555', 'JB010', 1, 'default.jpg'),
('1904445', 'ero', 'surabaya', '2019-04-18', 'L', 'jl.mangga"', '0488888', 'JB001', 1, 'default.jpg'),
('190466', 'rei', 'bandung', '1959-01-22', 'L', 'jjjj"', '09887766655', 'JB001', 1, 'default.jpg'),
('1904664', 'yaya', 'bandung', '2019-04-11', 'L', 'jl.kesemek""', '0488888', 'JB001', 1, 'default.jpg'),
('1905047', 'yaya', 'bandung', '2019-04-05', 'P', 'jl.elang1', '0488888', 'JB001', 1, 'default.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id_pembelian_d` int(11) NOT NULL,
  `id_pembelian_h` int(11) NOT NULL,
  `kode_barang` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id_pembelian_d`, `id_pembelian_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(1, 1, 'BR004', 3, 10000000, 10000000, 1),
(2, 2, 'BR003', 2, 30000, 30002, 1),
(3, 2, 'BR002', 20, 4000000, 4000020, 1),
(4, 5, 'BR005', 22, 12000000, 12000000, 1),
(5, 5, 'BR004', 3, 300000, 300003, 1),
(6, 3, 'BR001', 40, 2800000, 2800040, 1),
(7, 7, 'BR005', 8, 970000, 970008, 1),
(8, 7, 'BR003', 4, 190000, 190004, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_header`
--

CREATE TABLE `pembelian_header` (
  `id_pembelian_h` int(11) NOT NULL,
  `no_transaksi` varchar(10) NOT NULL,
  `tgl` date NOT NULL,
  `kode_supplier` varchar(5) NOT NULL,
  `approved` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_header`
--

INSERT INTO `pembelian_header` (`id_pembelian_h`, `no_transaksi`, `tgl`, `kode_supplier`, `approved`, `flag`) VALUES
(2, 'TR988', '2019-04-05', 'SP001', 1, 1),
(3, 'TR097', '2019-04-05', 'SP006', 1, 1),
(4, 'TR067', '2019-04-05', 'SP005', 1, 1),
(5, 'TR777', '2019-04-05', 'SP002', 1, 1),
(6, 'TR90410A01', '2019-04-05', 'SP007', 1, 1),
(7, 'TR90411B01', '2019-04-11', 'SP002', 1, 1),
(8, 'TR90415B98', '2019-04-23', 'SP003', 1, 1),
(9, 'TR90415B98', '2019-04-23', 'SP003', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id_jual_d` int(11) NOT NULL,
  `id_jual_h` int(11) NOT NULL,
  `kode_barang` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`id_jual_d`, `id_jual_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(1, 1, 'BR002', 3, 70000, 210000, 1),
(2, 1, 'BR004', 12, 1000000, 12000000, 1),
(3, 1, 'BR004', 5, 1000000, 5000000, 1),
(4, 2, 'BR006', 10, 10000000, 100000000, 1),
(5, 2, 'BR005', 3, 800000, 2400000, 1),
(6, 2, 'BR002', 6, 70000, 420000, 1),
(7, 3, 'BR003', 2, 360000, 720000, 1),
(8, 3, 'BR004', 5, 1000000, 5000000, 1),
(9, 3, 'BR007', 8, 560000, 4480000, 1),
(10, 4, 'BR006', 2, 10000000, 20000000, 1),
(11, 4, 'BR003', 3, 360000, 1080000, 1),
(12, 5, 'BR004', 2, 1000000, 2000000, 1),
(13, 5, 'BR006', 3, 10000000, 30000000, 1),
(14, 1, 'BR003', 2, 360000, 720000, 1),
(15, 2, 'BR006', 2, 10000000, -10000000, 1),
(16, 1, 'BR004', 2, 1000000, 2000000, 1),
(17, 1, 'BR001', 9, 800000, 7200000, 1),
(18, 1, 'BR001', 3, 800000, 2400000, 1),
(19, 1, 'BR001', 3, 800000, 2400000, 1),
(20, 1, 'BR001', 3, 800000, 2400000, 1),
(21, 1, 'BR001', 3, 800000, 2400000, 1),
(22, 1, 'BR001', 3, 800000, 2400000, 1),
(23, 1, 'BR001', 3, 800000, 2400000, 1),
(24, 1, 'BR001', 3, 800000, 2400000, 1),
(25, 1, 'BR002', 1, 70000, 70000, 1),
(26, 1, 'BR003', 3, 360000, 1080000, 1),
(27, 2, 'BR005', 2, 800000, 1600000, 1),
(28, 2, 'BR004', 3, 1000000, 3000000, 1),
(29, 2, 'BR006', 3, 10000000, 30000000, 1),
(30, 1, 'BR007', 5, 560000, 2800000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_header`
--

CREATE TABLE `penjualan_header` (
  `id_jual_h` int(11) NOT NULL,
  `no_transaksi` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `pembeli` varchar(250) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_header`
--

INSERT INTO `penjualan_header` (`id_jual_h`, `no_transaksi`, `tanggal`, `pembeli`, `flag`) VALUES
(1, 'TR098', '2019-04-18', 'yanti', 1),
(2, 'TR067', '2019-04-18', 'zakiy', 1),
(3, 'TR777', '2019-04-18', 'elang', 1),
(4, 'TR70511A01', '2019-04-18', 'reni', 1),
(5, 'TR80612B01', '2019-04-19', 'ratu', 1),
(6, '55555', '2019-04-22', 'YAYA', 1),
(7, 'tr433', '2019-04-22', 'zakiy', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `kode_supplier` varchar(5) NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`kode_supplier`, `nama_supplier`, `alamat`, `telp`, `flag`) VALUES
('SP001', 'Tania', 'jl.semangka raya no.56"', '087226246744', 1),
('SP002', 'CV.iing', 'jl.raya no 23"', '088866666666', 1),
('SP003', 'gilang', 'Jl.elang raya no.89"', '0215689389966', 1),
('SP004', 'ratu', 'jl.gajah mungkur', '0984554336666', 1),
('SP005', 'yanti', 'jl.yos sudarso no.78', '021345677883899', 1),
('SP006', 'yani', 'jl.ui no.89', '0488888', 1),
('SP007', 'dadu', 'jl.kalisari subang', '021345677883899', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `tipe` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nik`, `email`, `password`, `tipe`, `flag`) VALUES
(1, '134562578', 'user@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 2, 1),
(2, '166777777', 'admin@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`id_pembelian_d`);

--
-- Indexes for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  ADD PRIMARY KEY (`id_pembelian_h`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id_jual_d`);

--
-- Indexes for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  ADD PRIMARY KEY (`id_jual_h`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kode_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `id_pembelian_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  MODIFY `id_pembelian_h` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id_jual_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  MODIFY `id_jual_h` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
